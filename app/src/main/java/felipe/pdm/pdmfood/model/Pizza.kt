package felipe.pdm.pdmfood.model

import android.os.Parcel
import android.os.Parcelable

class Pizza(var name: String?, var value: Float?, var ingredients: String?) : Parcelable {
    constructor(parcel: Parcel) : this(
        name = parcel.readString(),
        value = parcel.readFloat(),
        ingredients = parcel.readString()
    ) {
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeString(name)
        dest?.writeFloat(value as Float)
        dest?.writeString(ingredients)
    }

    override fun describeContents(): Int {
        return 0;
    }

    companion object CREATOR : Parcelable.Creator<Pizza> {
        override fun createFromParcel(parcel: Parcel): Pizza {
            return Pizza(parcel)
        }

        override fun newArray(size: Int): Array<Pizza?> {
            return arrayOfNulls(size)
        }
    }


}