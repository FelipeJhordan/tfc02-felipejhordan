package felipe.pdm.pdmfood.ui.activities

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import felipe.pdm.pdmfood.R
import felipe.pdm.pdmfood.data.DAOPizzasSingleton
import felipe.pdm.pdmfood.model.Pizza


class PizzaMenuActivity : AppCompatActivity() {

    private lateinit var lnlvListPizza: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        this.lnlvListPizza = findViewById(R.id.lnlvPizzaMenu)

       this.buildListPizza()
    }

    private fun buildListPizza() {
        if(DAOPizzasSingleton.getPizzas().size != 0) {
            for (pizza: Pizza in DAOPizzasSingleton.getPizzas()) {
                val view: View = LayoutInflater.from(this)
                    .inflate(R.layout.pizza_item_view, this.lnlvListPizza, false)
                val txtPizzaDetailName: TextView = view.findViewById(R.id.txtPizzaName)
                txtPizzaDetailName.text = pizza.name
                val txtPizzaDetailValue: TextView = view.findViewById(R.id.txtPizzaValue)
                txtPizzaDetailValue.text = "$${pizza.value.toString()}"
                val txtPizzaDetailIngredients: TextView = view.findViewById(R.id.txtPizzaIngredients)
                txtPizzaDetailIngredients.text = pizza.ingredients
                val ivPizza: ImageView = view.findViewById(R.id.ivPizza)
                val set = setOf(R.drawable.pizza1, R.drawable.pizza2, R.drawable.pizza3, R.drawable.pizza4)
                ivPizza.setImageResource(set.elementAt((0..3).random()))
                view.tag = pizza
                view.setOnClickListener(View.OnClickListener {
                    var p: Pizza = it.tag as Pizza
                    showStoreInfo(p)
                })
                this.lnlvListPizza.addView(view)

            }
        } else {
            var txtNoPizzas: TextView = TextView(this)
            txtNoPizzas.text = "Não existe pizzas cadastradas."
            this.lnlvListPizza.addView(txtNoPizzas)
        }
    }

    private fun calculatePrice(value: Float): MutableMap<String, Float> {
        var map: MutableMap<String, Float> = mutableMapOf(Pair("M", value))
        map["P"] =  value * 0.25f
        map["G"] =  value * 1.25f
        println(map.toString())
        return map
    }

    private fun appendLabel( view: View , map: Map<String, Float>) {
        var txtP: TextView = view.findViewById(R.id.txtDialogPriceP)
        var txtM: TextView = view.findViewById(R.id.txtDialogPriceM)
        var txtG: TextView = view.findViewById(R.id.txtDialogPriceG)

        txtP.text = "$"+ map.get("P").toString()
        txtM.text = "$"+ map.get("M").toString()
        txtG.text = "$"+ map.get("G").toString()
    }

    private fun showStoreInfo(pizza: Pizza) {
        var dialogBuilder: AlertDialog.Builder = AlertDialog.Builder(this)
        dialogBuilder.setTitle(R.string.store_info)
        var view: View =
            LayoutInflater.from(this).inflate(R.layout.show_pizza_info_dialog, null, false)
        dialogBuilder.setView(view)
        var txtStoreName: TextView = view.findViewById(R.id.txtDialogName)
        txtStoreName.text = pizza.name
        var txtIngredients: TextView = view.findViewById(R.id.txtDialogIngredients)
        txtIngredients.text = pizza.ingredients
        appendLabel(view, calculatePrice(pizza.value!!))
        dialogBuilder.apply {
            setPositiveButton(R.string.btn_ok, DialogInterface.OnClickListener { dialog, which ->
            })
        }
        dialogBuilder.create().show()
    }
}