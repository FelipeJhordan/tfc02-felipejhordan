package felipe.pdm.pdmfood.ui.activities

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.TextView
import android.widget.Toast
import felipe.pdm.pdmfood.R
import felipe.pdm.pdmfood.model.Pizza
import java.lang.NumberFormatException

class NewPizzaActivity : AppCompatActivity() {

    companion object {
        val RESULT_KEY = "NewStoreActivity.RESULT_KEY"
    }

    private lateinit var etxtPizzaName: TextView
    private lateinit var etxtValue: TextView
    private lateinit var etxtIngredients: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_pizza)
        this.etxtPizzaName = findViewById(R.id.etxtPizzaName)
        this.etxtValue = findViewById(R.id.etxtValue)
        this.etxtIngredients = findViewById(R.id.etxtIngredients)
    }

    fun onClickSave(view: View) {
       try {
           var pizzaName: String = this.etxtPizzaName.text.toString()
           var value: Float = this.etxtValue.text.toString().toFloat()
           var ingredients: String = this.etxtIngredients.text.toString()
           if(pizzaName.isEmpty() || value.isNaN() || ingredients.isEmpty())  return
           else this.savePizza(Pizza(pizzaName, value, ingredients ))
           finish()
       } catch (e: NumberFormatException) {
           var toast: Toast = Toast.makeText(this, "Insira um preço válido", Toast.LENGTH_LONG)
           toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0)
           toast.show()
           return
       } catch(e: Exception){
           return
       }
    }

    private fun savePizza( pizza: Pizza) {
        val output: Intent = Intent()
        output.putExtra(RESULT_KEY, pizza)
        setResult(Activity.RESULT_OK, output)
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        super.onBackPressed()
    }
}