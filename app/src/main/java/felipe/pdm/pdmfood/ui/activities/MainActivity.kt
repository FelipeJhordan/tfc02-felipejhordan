package felipe.pdm.pdmfood.ui.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import felipe.pdm.pdmfood.R
import felipe.pdm.pdmfood.data.DAOPizzasSingleton
import felipe.pdm.pdmfood.model.Pizza

class MainActivity: AppCompatActivity() {

    companion object  {
        const val FORM_REQUEST_CODE = 1578
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DAOPizzasSingleton.getPizzas()
        setContentView(R.layout.activity_main)
    }

    fun onClickNewStore(view: View) {
          var openStoreForms: Intent = Intent(this, NewPizzaActivity::class.java)
          startActivityForResult(openStoreForms, FORM_REQUEST_CODE)
    }

    fun onClickListStores(view: View) {
        var listAllStores = Intent(this, PizzaMenuActivity::class.java)
        startActivity(listAllStores)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if( (requestCode == FORM_REQUEST_CODE) && (resultCode == RESULT_OK) && (data != null) ) {
             var store: Pizza? = data.getParcelableExtra<Pizza>(NewPizzaActivity.RESULT_KEY)
             DAOPizzasSingleton.addPizza(store as Pizza) // store or null também funcionaria ( mas estaria + feio o código )

        }
    }

}