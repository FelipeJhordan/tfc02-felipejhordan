package felipe.pdm.pdmfood.data

import felipe.pdm.pdmfood.model.Pizza

object DAOPizzasSingleton {
      private var pizzas: ArrayList<Pizza> = ArrayList<Pizza>()

     fun getPizzas():ArrayList<Pizza> {
          return this.pizzas
     }

     fun addPizza(pizza:Pizza) {
          this.pizzas.add(pizza)
     }
}